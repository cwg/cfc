/*
 * This file is part of the cfc package.
 *
 * Written 2010 by Christoph Groth <cwg@falma.de>.
 *
 * This file has been placed in the public domain.
 */

#include <iostream>
#include <cmath>
#include <cfc/threadsafe_ostream.hh>
#include <omp.h>

long long wasteful_square(long long a)
{
    long long sum = 0;
    for (long long i = 1; i <= a; ++i)
        sum += i;
    return 2 * sum - a;
}

int main()
{
    omp_lock_t lock;
    omp_init_lock(&lock);

#pragma omp parallel
    {
        // Small bufsize for demonstration purposes
        cfc::Threadsafe_ostream ts_cout(std::cout, &lock, 1000);
#pragma omp for
        for (int i = 0; i < 1000; ++i) {
            int num = 10000000 + i;
            ts_cout << "The square of " << num << " is "
                    << wasteful_square(num) << '\n' << std::flush;
            // flush means "end of chunk".  Call ts_cout.really_flush() for a
            // real flush.
        }
    }
}
