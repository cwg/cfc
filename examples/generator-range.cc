/*
 * This file is part of the cfc package.
 *
 * Written 2009 by Christoph Groth <cwg@falma.de>.
 *
 * This file has been placed in the public domain.
 */

#include <iostream>
#include <cassert>
#include <cfc/generator.hh>

// A python-like range

class Range : public cfc::Generator<int, Range> {
public:
    Range(int _end) : end(_end), step(1) {
        result = 0;
        operator()();
    }
    Range(int start, int _end)
        : end(_end), step(1) {
        result = start;
        operator()();
    }
    Range(int start, int _end, int _step)
        : end(_end), step(_step) {
        assert(step != 0);
        result = start;
        operator()();
    }
    void operator()();
private:
    const int end, step;
};

void Range::operator()()
{
    GENERATOR_BEGIN;
    if (step > 0)
        for (; result < end; result += step)
            YIELD_result;
    else
        for (; result > end; result += step)
            YIELD_result;
    GENERATOR_END;
}

int main()
{
    for (Range i(0, 20, 3); i; ++i)
        std::cout << *i << '\n';
    return 0;
}
