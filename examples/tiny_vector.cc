/*
 * This file is part of the cfc package.
 *
 * Written 2009 by Christoph Groth <cwg@falma.de>.
 *
 * This file has been placed in the public domain.
 */

#include <iostream>
#include <cfc/tiny_vector.hh>
#include <cmath>

typedef cfc::Tiny_vector<double, 3> Vec3d;

int main()
{
    Vec3d a(1, 2, 3), b(2, 3, 4);
    Vec3d c = cross(a, b);
    c /= std::sqrt(dot(c, c));
    std::cout << "Normalized vector perpendicular to " << a << " and "
              << b << ":\n" << c << '\n';
}
