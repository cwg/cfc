/*
 * This file is part of the cfc package
 *
 * Copyright 2009, 2010 Christoph Groth <cwg@falma.de>
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation; either version 2.1 of the License, or (at your
 * option) any later version.
 */

#include <cassert>
#include <cfc/threadsafe_ostream.hh>

cfc::Threadsafe_outbuf::Threadsafe_outbuf(std::ostream &_ostr,
                                          omp_lock_t *_lock, long bufsize)
    : ostr(_ostr), lock(_lock)
{
    assert(bufsize > 0);
    filled_beg = last_sync = buf_beg = new char[bufsize];
    buf_end = buf_beg + bufsize;
    setp(buf_beg, buf_end);
}

cfc::Threadsafe_outbuf::~Threadsafe_outbuf()
{
    sync();
    overflow(std::char_traits<char>::eof());
    delete[] buf_beg;
}

cfc::Threadsafe_outbuf::int_type cfc::Threadsafe_outbuf::overflow(int_type c)
{
    // If there are some chunks to write, do that.  The second term is used to
    // distinguish a completely full buffer from a completely empty one.
    if (last_sync != filled_beg || epptr() == filled_beg) {
        // Write everything up to last sync.
        omp_set_lock(lock);
        if (filled_beg < last_sync) {
            ostr.write(filled_beg, last_sync - filled_beg);
            filled_beg = last_sync;
        } else {
            size_t t = buf_end - filled_beg;
            if (t > 0) ostr.write(filled_beg, t);
            ostr.write(buf_beg, last_sync - buf_beg);
        }
        omp_unset_lock(lock);
    }

    // Identify new region of buffer to be used by std::streambuffer methods.
    if (pptr() < last_sync)
        setp(pptr(), last_sync);
    else if (pptr() < buf_end)
        setp(pptr(), buf_end);
    else
        setp(buf_beg, last_sync);

    // Handle wrapping of the buffer.
    if (last_sync == buf_end) last_sync = buf_beg;
    filled_beg = last_sync;

    if (c != std::char_traits<char>::eof()) {
        sputc(c);
    }
    return c;
}

cfc::Threadsafe_outbuf::int_type cfc::Threadsafe_outbuf::sync()
{
    last_sync = pptr();
    return 0;
}
