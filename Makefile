########################################################################
# This file is part of the cfc package
#
# Written 2009, 2010 by Christoph Groth <cwg@falma.de>.
#
# This file has been placed in the public domain.
########################################################################

# Usage:
# `make' or `make all' compiles the program
# `make clean' deletes files that can be remade

####### Definitions (might have to be tweaked, possibly in Makefile.conf)

OPT             = -O3
WARN            = -Wall -Wextra
MACROS          =

CXX             = g++
AR              = ar rcs
CXXFLAGS        = -g -fopenmp -I. $(WARN) $(OPT) $(MACROS)

########################################################################
####### You should not have to change anything beyond this line. #######
########################################################################

# You can create a Makefile.conf to override any defaults.
ifeq ($(wildcard Makefile.conf), Makefile.conf) # = if exists
include Makefile.conf
endif

####### Source files

LIB_SRC = $(wildcard src/*.cc)
LIB_OBJ =$(LIB_SRC:.cc=.o)
EX_SRC = $(wildcard examples/*.cc)
EX_TARGETS = $(EX_SRC:.cc=)

ALL_SRC = $(sort $(LIB_SRC) $(EX_SRC))
ALL_DEP = $(ALL_SRC:.cc=.d)

ALL_TARGETS = $(EX_TARGETS) libcfc.a

# You can create a Makefile.conf to override any defaults.
ifeq ($(wildcard Makefile.conf), Makefile.conf) # = if exists
include Makefile.conf
endif

####### Rules

# Delete unfinished files
.DELETE_ON_ERROR:

.PHONY: all
all: $(ALL_TARGETS)

.PHONY: examples
examples: $(EX_TARGETS)

# Build library
libcfc.a: $(LIB_OBJ)
	$(AR) libcfc.a $(LIB_OBJ)

# Advanced^2 Auto-Dependency Generation (simplified by cwg).
# http://thread.gmane.org/gmane.comp.gnu.make.general/4201
%.o:    %.cc %.d
	$(CXX) $(CPPFLAGS) $(CXXFLAGS) -MMD -MP -c -o $@ $<

%: %.cc %.d
	$(CXX) $(CPPFLAGS) $(CXXFLAGS) $(LDFLAGS) -MMD -MP -o $@ $<

# empty command: if a .d-file is missing, consider it as present and new.
# Togeter with the previous rule this will force compilation of file.cc if
# file.d is missing.
%.d: ;

examples/threadsafe_ostream: examples/threadsafe_ostream.cc libcfc.a
	$(CXX) $(CPPFLAGS) $(CXXFLAGS) $(LDFLAGS) -MMD -MP -L. \
-o examples/threadsafe_ostream examples/threadsafe_ostream.cc -lcfc

# Clean almost everything.
.PHONY: clean
clean:
	-rm -f $(ALL_TARGETS) $(LIB_OBJ) $(ALL_DEP)

-include $(ALL_DEP)
