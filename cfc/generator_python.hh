/*
 * This file is part of the cfc package
 *
 * Copyright 2010 Christoph Groth <cwg@falma.de>
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation; either version 2.1 of the License, or (at your
 * option) any later version.
 */

// A wrapper to expose cfc generators as python iterators using Boost.Python.
//
// Usage example:
//
// Suppose you have a Generator called Range whose constructor takes an int.
// To expose it just say:
//
// #include <cfc/generator_python.hh>
//
// BOOST_PYTHON_MODULE(kernel)
// {
//    cfc::Generator_wrapper<Range>::wrap("Range", boost::python::init<int>());
// }

#ifndef CFC_GENERATOR_PYTHON_HH
#define CFC_GENERATOR_PYTHON_HH

#include <boost/python.hpp>

namespace cfc {

// based on: http://wiki.python.org/moin/boost.python/iterator
inline boost::python::object return_self(boost::python::object const& self)
{
    return self;
}

template <class Generator>
struct Generator_wrapper {
    static const typename Generator::Result_type next(Generator& gen) {
        if (!gen) {
            PyErr_SetString(PyExc_StopIteration, "");
            boost::python::throw_error_already_set();
        }
        typename Generator::Result_type res = *gen;
        ++gen;
        return res;
    }

    template <class Init>
    static void wrap(const char *python_name, Init i) {
        boost::python::class_<Generator>(python_name, i)
            .def("next", next)
            .def("__iter__", return_self)
            ;
    }
};

}

#endif // CFC_GENERATOR_PYTHON_HH
