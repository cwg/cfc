/*
 * This file is part of the cfc package
 *
 * Copyright 2009, 2010 Christoph Groth <cwg@falma.de>
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation; either version 2.1 of the License, or (at your
 * option) any later version.
 */

// A very simple vector class optimized for low dimensional vectors.  See
// examples/tiny_vector.cc.

#ifndef CFC_TINY_VECTOR_HH
#define CFC_TINY_VECTOR_HH

#include <iostream>
#include <boost/static_assert.hpp>

namespace cfc {

template <class T, int D>
class Tiny_vector {
public:
    Tiny_vector();
    Tiny_vector(T _x);
    template <class Array> explicit Tiny_vector(Array _x);
    Tiny_vector(T x0, T x1);
    Tiny_vector(T x0, T x1, T x2);
    Tiny_vector(T x0, T x1, T x2, T x3);
    Tiny_vector(T x0, T x1, T x2, T x3, T x4);

    T &operator[](unsigned i) { return x[i]; }
    const T &operator[](unsigned i) const { return x[i]; }

    Tiny_vector<T, D> &operator+=(const Tiny_vector<T, D> &y);
    Tiny_vector<T, D> &operator-=(const Tiny_vector<T, D> &y);
    Tiny_vector<T, D> &operator*=(const T &y);
    Tiny_vector<T, D> &operator*=(const Tiny_vector<T, D> &y);
    Tiny_vector<T, D> &operator/=(const T &y);
    Tiny_vector<T, D> &operator/=(const Tiny_vector<T, D> &y);

    template <class Scalar_function>
    Tiny_vector<T, D> &map(Scalar_function sf);

private:
    T x[D];
};

template <class T, int D>
Tiny_vector<T, D>::Tiny_vector()
{
    // The following is necessary to suppress an unjustified warning of gcc
    // 4.4.4 about x being used uninitialized.
    if (D == 1) x[0] = 0;
}

template <class T, int D>
Tiny_vector<T, D>::Tiny_vector(T _x)
{
    for (int i = 0; i < D; ++i) x[i] = _x;
}

template <class T, int D>
template <class Array>
Tiny_vector<T, D>::Tiny_vector(Array _x)
{
    for (int i = 0; i < D; ++i) x[i] = _x[i];
}

template <class T, int D>
Tiny_vector<T, D>::Tiny_vector(T x0, T x1)
{
    BOOST_STATIC_ASSERT(D == 2);
    x[0] = x0;
    x[1] = x1;
}

template <class T, int D>
Tiny_vector<T, D>::Tiny_vector(T x0, T x1, T x2)
{
    BOOST_STATIC_ASSERT(D == 3);
    x[0] = x0;
    x[1] = x1;
    x[2] = x2;
}

template <class T, int D>
Tiny_vector<T, D>::Tiny_vector(T x0, T x1, T x2, T x3)
{
    BOOST_STATIC_ASSERT(D == 4);
    x[0] = x0;
    x[1] = x1;
    x[2] = x2;
    x[3] = x3;
}

template <class T, int D>
Tiny_vector<T, D>::Tiny_vector(T x0, T x1, T x2, T x3, T x4)
{
    BOOST_STATIC_ASSERT(D == 5);
    x[0] = x0;
    x[1] = x1;
    x[2] = x2;
    x[3] = x3;
    x[4] = x4;
}

template <class T, int D>
Tiny_vector<T, D> &Tiny_vector<T, D>::operator+=(const Tiny_vector &y)
{
    for (int i = 0; i < D; ++i) x[i] += y[i];
    return *this;
}

template <class T, int D>
Tiny_vector<T, D> &Tiny_vector<T, D>::operator-=(const Tiny_vector &y)
{
    for (int i = 0; i < D; ++i) x[i] -= y[i];
    return *this;
}

template <class T, int D>
Tiny_vector<T, D> &Tiny_vector<T, D>::operator*=(const T &y)
{
    for (int i = 0; i < D; ++i) x[i] *= y;
    return *this;
}

template <class T, int D>
Tiny_vector<T, D> &Tiny_vector<T, D>::operator*=(const Tiny_vector &y)
{
    for (int i = 0; i < D; ++i) x[i] *= y[i];
    return *this;
}

template <class T, int D>
Tiny_vector<T, D> &Tiny_vector<T, D>::operator/=(const T &y)
{
    for (int i = 0; i < D; ++i) x[i] /= y;
    return *this;
}

template <class T, int D>
Tiny_vector<T, D> &Tiny_vector<T, D>::operator/=(const Tiny_vector &y)
{
    for (int i = 0; i < D; ++i) x[i] /= y[i];
    return *this;
}

template <class T, int D>
std::ostream &operator<<(std::ostream &s, const Tiny_vector<T, D> &a)
{

    for (int i = 0; i < D - 1; ++i) s << a[i] << ' ';
    s << a[D - 1];
    return s;
}

template <class T, int D>
Tiny_vector<T, D> operator+(Tiny_vector<T, D> a, const Tiny_vector<T, D> &b)
{
    return a += b;
}

template <class T, int D>
Tiny_vector<T, D> operator-(Tiny_vector<T, D> a, const Tiny_vector<T, D> &b)
{
    return a -= b;
}

template <class T, int D, class O>
Tiny_vector<T, D> operator*(Tiny_vector<T, D> a, const O &b)
{
    return a *= b;
}

template <class T, int D>
Tiny_vector<T, D> operator*(const T &a, Tiny_vector<T, D> b)
{
    return b *= a;
}

template <class T, int D, class O>
Tiny_vector<T, D> operator/(Tiny_vector<T, D> a, const O &b)
{
    return a /= b;
}

template <class T>
Tiny_vector<T, 3> cross(const Tiny_vector<T, 3> &a, const Tiny_vector<T, 3> &b)
{
    return Tiny_vector<T, 3>(a[1] * b[2] - a[2] * b[1],
                             a[2] * b[0] - a[0] * b[2],
                             a[0] * b[1] - a[1] * b[0]);
}

template <class T, int D>
T dot(const Tiny_vector<T, D> &a, const Tiny_vector<T, D> &b)
{
    T ret = 0;
    for (int i = 0; i < D; ++i) ret += a[i] * b[i];
    return ret;
}

template<class T, int D> template<class Scalar_function>
Tiny_vector<T, D>& Tiny_vector<T, D>::map(Scalar_function sf)
{
    for (int i = 0; i < D; ++i) x[i] = sf(x[i]);
    return *this;
}

} // namespace cfc

#endif // CFC_TINY_VECTOR_HH
