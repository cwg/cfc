/*
 * This file is part of the cfc package
 *
 * Copyright 2009, 2010 Christoph Groth <cwg@falma.de>
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation; either version 2.1 of the License, or (at your
 * option) any later version.
 */

// This header defines the class Threadsafe_ostream which is a buffered ostream
// for OpenMP.  It is meant for cases where several threads want to
// asynchronously write chunks of data to the same ostream and it does not
// matter in which order the chunks end up in the ostream.
//
// The purpose of this class is to
//
// (1) guarantee that chunks output by from different threads don't get
// intertwingled
//
// (2) buffer the output in memory such that the ultimately necessary locking
// does not significantly affect performance
//
// Chunks are defined by flushing the Threadsafe_ostream.  A real flush of all
// finished chunks can be forced with really_flush().

#ifndef CFC_THREADSAFE_OSTREAM_HH
#define CFC_THREADSAFE_OSTREAM_HH

#include <iostream>
#include <omp.h>

namespace cfc {

// A streambuffer based FIFO.
class Threadsafe_outbuf : public std::streambuf {
public:
    Threadsafe_outbuf(std::ostream &_ostr, omp_lock_t *_lock, long bufsize);
    virtual ~Threadsafe_outbuf();

    void really_flush() {
        overflow(std::char_traits<char>::eof());
    }
protected:
    char *buf_beg, *buf_end, *filled_beg, *last_sync;
    std::ostream &ostr;
    omp_lock_t *lock;

    virtual int_type overflow(int_type c);
    virtual int_type sync();
};

class Threadsafe_ostream : public std::ostream {
public:
    // Construct an instance of Threadsafe_ostream bound to ostream `ostr',
    // using OpenMP lock `lock' and buffering at most `bufsize' chars.  The
    // buffer must be big enough for the largest expected chunk.  Otherwise
    // chunk consistency is not guaranteed! (This is not detected)
    Threadsafe_ostream(std::ostream &ostr, omp_lock_t *lock,
                       long bufsize = 64 * 1024)
        : std::ostream(&outbuf), outbuf(ostr, lock, bufsize) {
    }
    // Pass on any finished chunks to the associated ostream.
    void really_flush() {
        outbuf.really_flush();
    }
protected:
    Threadsafe_outbuf outbuf;
};

} // namespace cfc

#endif // CFC_THREADSAFE_OSTREAM_HH
