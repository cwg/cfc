/*
 * This file is part of the cfc package
 *
 * Copyright 2009, 2010 Christoph Groth <cwg@falma.de>
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation; either version 2.1 of the License, or (at your
 * option) any later version.
 */

// An implementation of python-like generators with an iterator interface.
// This relies on standard C++ but contains a more efficient specialization for
// GCC.
//
// To make a generator, derive a class from Generator<> and provide a
// operator() member function.  If your operator does not take any parameters
// you can use the generator like an iterator, for example:
//
// for (My_generator g(parameter); g; ++g) cout << *g;
//
// See examples/generator-range.cc for a full example.
//
// Based on ideas from:
// * "Coroutines in C" by Simon Tatham
//   http://www.chiark.greenend.org.uk/~sgtatham/coroutines.html
// * "Generators in C++" by Andrew Fedoniouk
//   http://www.codeproject.com/KB/cpp/cpp_generators.aspx
// * "Secret sauce revealed" by Chris Kohlhoff
//   http://blog.think-async.com/2009/08/secret-sauce-revealed.html

#ifndef CFC_GENERATOR_HH
#define CFC_GENERATOR_HH

#include <cassert>

namespace cfc {

template<class Result, class Derived>
class Generator
{
public:
    typedef Result Result_type;
    // static polymorphism
    Derived &operator++() {
        assert(_line != 0);
        static_cast<Derived*>(this)->operator()();
        return *static_cast<Derived*>(this);
    }
    const Result& operator*() const { assert(_line != 0); return result; }
    const Result* operator->() const { assert(_line != 0); return &result; }
    operator bool() const { return _line != 0; }
protected:
    Generator() : _line(0) { }
    Result result;
#if defined(__GNUC__) && !defined(__INTEL_COMPILER)
    void *_line;                // use pointers to labels
#else
    int _line;                  // use switch
#endif
private:
};

}

#if defined(__GNUC__) && !defined(__INTEL_COMPILER)
// The compiler is gcc.

// The following two macros are necessary, because _GENERATOR_LABEL2(__LINE__)
// evaluates to __generator_label___LINE__ and only _GENERATOR_LABEL(__LINE__)
// evalauats to (for example) _generator_label_123.
#define _GENERATOR_LABEL2(LINE) _generator_label_##LINE
#define _GENERATOR_LABEL(LINE) _GENERATOR_LABEL2(LINE)

#define GENERATOR_BEGIN if (this->_line) goto *this->_line

#define GENERATOR_END  this->_line = 0

// The for loop is a clever trick to first evaluate the argument and
// subsequently set _line to __LINE__ and return.
#define YIELD                                                   \
    if (false) {                                                \
        _GENERATOR_LABEL(__LINE__): ;                           \
    } else                                                      \
        for (bool _internal = false;; _internal = !_internal)   \
            if (_internal) {                                    \
                this->_line = &&_GENERATOR_LABEL(__LINE__);     \
                return;                                         \
            } else this->result =

#define YIELD_result                            \
    do {                                        \
        __label__ _local_label;                 \
        this->_line = &&_local_label;           \
        return;                                 \
    _local_label: ;                             \
    } while (false)

#else
// The compiler is not gcc.

#define GENERATOR_BEGIN switch(this->_line) { case 0:

#define GENERATOR_END  } this->_line = 0

// The for loop is a clever trick to first evaluate the argument and
// subsequently set _line to __LINE__ and return.
#define YIELD                                                   \
    if (false) {                                                \
    case __LINE__: ;                                            \
    } else                                                      \
        for (bool _internal = false;; _internal = !_internal)   \
            if (_internal) {                                    \
                this->_line = __LINE__;                         \
                return;                                         \
            } else this->result =

// This macro can be used instead of CFC_YIELD if the result to be yielded is
// already in the `result' member variable.  It saves one copy.
#define YIELD_result                            \
    do {                                        \
        this->_line = __LINE__;                 \
        return;                                 \
    case __LINE__: ;                            \
    } while (false)

#endif

#endif // CFC_GENERATOR_HH
